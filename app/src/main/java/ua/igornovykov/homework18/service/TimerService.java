package ua.igornovykov.homework18.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import ua.igornovykov.homework18.manager.IProgressListener;
import ua.igornovykov.homework18.manager.NotificationManager;
import ua.igornovykov.homework18.ui.screen.MainActivity;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class TimerService extends Service {
    public static final int WORK_COUNTER = 50;

    private long startTime;

    private IProgressListener listener;
    private ExecutorService executorService = Executors.newFixedThreadPool(1);

    @Override
    public void onCreate() {
        super.onCreate();
        startTime = System.currentTimeMillis();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        executorService.execute(new Notify());
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    class Notify implements Runnable {
        long timeDelta;
        int counter = 0;
        Intent intent = new Intent(MainActivity.BROADCAST_ACTION);

        @Override
        public void run() {
            setListener(new NotificationManager(TimerService.this));
            while (counter <= WORK_COUNTER) {
                timeDelta = System.currentTimeMillis() - startTime;
                intent.putExtra(MainActivity.PARAM_TIME, timeDelta);
                listener.progressListener(counter++);

                try{
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e){
                    e.printStackTrace();
                }
            }
        }
    }

    public void setListener(IProgressListener listener) {
        this.listener = listener;
    }

}
