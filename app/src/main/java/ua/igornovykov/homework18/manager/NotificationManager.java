package ua.igornovykov.homework18.manager;

import android.app.Notification;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;

import ua.igornovykov.homework18.R;
import ua.igornovykov.homework18.service.TimerService;

public class NotificationManager implements IProgressListener {

    private final int TIMER_NOTIFICATION = 102;
    private int counter = 0;

    private Context context;
    private android.app.NotificationManager notificationManager;
    private NotificationCompat.Builder builder;

    public NotificationManager(Context context) {
        this.context = context;
        notificationManager = (android.app.NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        builder = new NotificationCompat.Builder(context);
    }

    @Override
    public void progressListener(long progress) {
        counter = (int) progress;
        notificationManager.notify(TIMER_NOTIFICATION, createNotification());
    }

    private Notification createNotification() {
        builder.setContentTitle("Downloading")
                .setWhen(System.currentTimeMillis())
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setTicker("Download started")
                .setContentText("Download progress " + (counter * 2) + "%")
                .setProgress(TimerService.WORK_COUNTER, counter, false);
        return builder.build();
    }
}
