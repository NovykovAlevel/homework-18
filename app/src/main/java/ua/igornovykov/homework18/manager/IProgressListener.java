package ua.igornovykov.homework18.manager;

public interface IProgressListener {
    void progressListener(long progress);
}
