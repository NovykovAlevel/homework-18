package ua.igornovykov.homework18.ui.screen;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import ua.igornovykov.homework18.R;
import ua.igornovykov.homework18.service.TimerService;

public class MainActivity extends AppCompatActivity {

    public final static String PARAM_TIME = "time";
    public final static String BROADCAST_ACTION = "com.example.servicebindingsample";

    TextView timer;
    BroadcastReceiver broadcastReceiver;
    LocalBroadcastManager broadcastManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        timer = findViewById(R.id.txt_timer);

        Intent intent = new Intent(this, TimerService.class);
        startService(intent);

        broadcastManager = LocalBroadcastManager.getInstance(getApplicationContext());

        broadcastReceiver = new BroadcastReceiver() {
            // действия при получении сообщений
            @Override
            public void onReceive(Context context, Intent intent) {
                long timeDelta = intent.getLongExtra(PARAM_TIME, 0);
                timer.setText(String.valueOf(timeDelta));
            }
        };

        // создаем фильтр для BroadcastReceiver
        IntentFilter intentFilter = new IntentFilter(BROADCAST_ACTION);
        // регистрируем (включаем) BroadcastReceiver
        broadcastManager.registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }

    ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d("wtf", "onServiceConnected: ");
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d("wtf", "onServiceDisconnected: ");
        }
    };


}
